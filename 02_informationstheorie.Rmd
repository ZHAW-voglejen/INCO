---
title: "INCO 02 Informationstheorie"
output: html_notebook
---


# Datenquelle

Definiert durch $X_k$ wobei z.B. $k = [0,1,2,...]$. Eine Datenquelle ist ein Strom von Ereignissen, der Periodisch zu jedem Zeitpunkt $k$ eine zufällige Nachricht abgibt.

## Beispiel:

**Wetter Prognose:** $L_k$ mit $k = [\text{gutes Wetter, schlechtes Wetter, wechselhaftes Wetter}]$


# Ereignisse

Ein Ereignis (z.B. $X_1$) ist ein bestimmte Nachricht aus einer Datenquelle.

## Beispiel:

**Wetter Prognose:** $L_1 = \text{gutes wetter}$


# Wahrscheinlichkeiten

Jede Nachricht hat eine Warscheinlichkeit als Ereignis vorzukommen. Diese wird definiert durch ($x_n$ ist eine bestimmte Nachricht) $P(x_n)$. $P(x_n)$ ist ein Wert zwischen 0 und 1 welche die Warscheinlichkeit angibt.

## Beispiel:

**Wetter Progonose:**

$$
P(w_0) = 0.28 \\
P(w_1) = 0.26 \\
P(w_2) = 0.46
$$

Die Summe der Wahrscheinlichkeiten aller Nachrichten sollte immer $1$ sein.


# Informationsgehalt (Relevanz der Information)

Nachrichten mit tiefer Wahrscheinlichkeit haben einen höheren Informationsgehalt. Aus dem Satz kann man die folgende Formel ableiten:  
$$
I(x_n) \approx \frac{1}{P(x_n)}
$$

Damit man nicht zu grosse Zahlen bekommt und besser Codieren kann gibt es die "Bit" Formel. Folgendes wird definiert als "Bit":  
$$
I(x_n) = \log_2 \frac{1}{P(x_n)}
$$

## Eigenschaften

Alle Ereignisse haben dieselbe Auftretenwarscheinlichkeit $P(x_n) = \frac{1}{N}$ dann ist der Informationsgehalt aller Ereignisse:  
$$
I(x_n) = \log_2{N}
$$


Minimaler Wert $I_{min}$ ist beim Ereignis welches praktisch sicher auftritt.
Wahrscheinlichkeit $P(x_n) \rightarrow 1$  
$$
I_{min} = \lim_{P(x_n)\rightarrow 1} \log_2{\frac{1}{P(x_n)}} \rightarrow 0
$$

Maximaler Wert $I_{max}$ ist beim Ereignis welches praktisch nie auftritt.
Warscheinlichkeit $P{x_n} \rightarrow 0$  
$$
I_{min} = \lim_{P(x_n)\rightarrow 0} \log_2{\frac{1}{P(x_n)}} \rightarrow \infty
$$





















